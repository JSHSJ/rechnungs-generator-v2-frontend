export const state = () => ({
    flashMessage: ""
})

export const mutations = {
    reset_flash (state) {
        state.flashMessage = ""
    },
    set_flash(state, flashMessage) {
        state.flashMessage = flashMessage
    }

}

export const actions = {
    async put_flash_message({commit}, message) {
        commit("set_flash", message)
    }
}