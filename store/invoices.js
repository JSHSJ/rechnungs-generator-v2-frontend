import axios from "axios";

export const state = () => ({
    list: [],
    activeInvoice: {},
})

export const mutations = {
    set_invoices (state, invoices) {
        state.list = invoices
    },

    set_active(state, number) {
        let invoice = state.list.filter(inv => {
            return inv.number == number
        })[0]

        state.activeInvoice = invoice
    },
    set_active_invoice(state, invoice) {
        state.activeInvoice = invoice
    },

    // UPDATE ACTIVE INVOICES

    updateHeadline (state, value) {
        state.activeInvoice.headline = value
    },
    updateNumber (state, value) {
        state.activeInvoice.number = value
    },
    updateDate (state, value) {
        state.activeInvoice.date = value
    },
    updateDaysToPay (state, value) {
        state.activeInvoice.daysToPay = value
    },
    updateRecipientName (state, value) {
        state.activeInvoice.recipientName = value
    },
    updateRecipientAddress (state, value) {
        state.activeInvoice.recipientAddress = value
    },
    updateRecipientCity (state, value) {
        state.activeInvoice.recipientCity = value
    },
    updateJobs (state, value) {
        state.activeInvoice.jobs = value
    },
    updateTotal (state, value) {
        state.activeInvoice.total = value
    },
    updateNotes(state, value) {
        state.activeInvoice.notes = value
    },
    updatePhrase(state, value) {
        state.activeInvoice.phrase = value
    },
    updateName (state, value) {
        state.activeInvoice.name = value
    },
    updateContactName (state, value) {
        state.activeInvoice.contactName = value
    },
    updateContactAddress (state, value) {
        state.activeInvoice.contactAddress = value
    },
    updateContactCity (state, value) {
        state.activeInvoice.contactCity = value
    },
    updateContactPhone (state, value) {
        state.activeInvoice.contactPhone = value
    },
    updateContactMail (state, value) {
        state.activeInvoice.contactMail = value
    },
    updateContactBankName (state, value) {
        state.activeInvoice.contactBankName = value
    },
    updateContactIBAN (state, value) {
        state.activeInvoice.contactIBAN = value
    },
    updateContactBIC (state, value) {
        state.activeInvoice.contactBIC = value
    },
    updateContactUST (state, value) {
        state.activeInvoice.contactUST = value
    },

    updateJobName (state, {id, value}) {
        const job = state.activeInvoice.jobs.find(job => job.id === id);
        job.name = value
    },

    updateJobText (state, {id, value}) {
        const job = state.activeInvoice.jobs.find(job => job.id === id);
        job.text = value
    },

    updateJobOptions (state, {id, value}) {
        const job = state.activeInvoice.jobs.find(job => job.id === id);
        job.options = value
    },

    updateNoteText (state, {id, value}) {
        const note = state.activeInvoice.notes.find(note => note.id === id);
        note.text = value
    },

    // UPDATE

    add_job(state) {
        if (state.activeInvoice.jobs == null) {
            state.activeInvoice.jobs = []
        }

        let id = findID(state.activeInvoice.jobs)

        state.activeInvoice.jobs.push({
            id: id,
            name: "",
            text: "",
            options: ""
        })
    },

    add_note(state) {
        if (state.activeInvoice.notes == null) {
            state.activeInvoice.notes = []
        }
        let id = findID(state.activeInvoice.notes)

        state.activeInvoice.notes.push({
            id: id,
            text: "",
        })
    }
}

export const getters = {
    getJobByID: (state) => (id) => {
        return state.activeInvoice.jobs.find(job => job.id === id);
    },
    getJobNameByID: (state) => (id) => {
        let job = state.activeInvoice.jobs.find(job => job.id === id);
        return job.name
    },
    getJobTextByID: (state) => (id) => {
        let job = state.activeInvoice.jobs.find(job => job.id === id);
        return job.text
    },
    getJobOptionsByID: (state) => (id) => {
        let job = state.activeInvoice.jobs.find(job => job.id === id);
        return job.options
    },
    getNoteTextByID: (state) => (id) => {
        let note = state.activeInvoice.notes.find(note => note.id === id);
        return note.text
    }
}

export const actions = {
    async get_invoices({commit}) {
        const { data } = await axios.get("http://localhost:8080/api/invoices")
        commit('set_invoices', data)
    },
    async new_invoice({ commit }) {
        const { data } = await axios.get("http://localhost:8080/api/invoices/new")
        commit("set_active_invoice", data)
    },
    async create_invoice({ dispatch, state }) {
        const { data } = await axios.post("http://localhost:8080/api/invoices", state.activeInvoice
            , {
            'Content-Type' : 'application/json; charset=UTF-8',
        })
        let message = `Invoice ${data.name} has been created successfully.`
        dispatch("put_flash_message", message, {root: true})
        await dispatch("get_invoices")
    },
}

function findID(arr) {
    let id = getNextID(arr, arr.length)
    return id
}


function getNextID(arr, id) {
    if (isUniqueID(arr, id)) {
        return id
    } else {
        id++;
        return getNextID(arr, id)
    }
}

function isUniqueID(arr, id) {
    let contains = arr.filter(elem => {
        return elem.id === id
    })
    let unique = contains.length === 0;
    return unique;
}
