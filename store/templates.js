import axios from "axios";

export const state = () => ({
    list: []
})

export const mutations = {
    set_templates (state, templates) {
        state.list = templates
    }
}

export const actions = {
    async get_templates({commit}) {
        const { data } = await axios.get("http://localhost:8080/api/templates")
        commit('set_templates', data)
    },
    async create_template({ dispatch, rootState }, templateName) {
        let template = rootState.invoices.activeInvoice
        const { data } = await axios.post("http://localhost:8080/api/templates", {
            template: template,
            templateName: templateName
    }
            , {
            'Content-Type' : 'application/json; charset=UTF-8',
        })
        let message = `Template ${data.templateName} has been created successfully.`
        dispatch("put_flash_message", message, {root: true})
        await dispatch("get_templates")
    },
}
