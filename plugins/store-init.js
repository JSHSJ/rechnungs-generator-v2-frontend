export default async context => {
    await context.store.dispatch('invoices/get_invoices', context)
    await context.store.dispatch('templates/get_templates', context)
}